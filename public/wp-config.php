<?php

defined('WP_ONLY') or (require __DIR__.'/index.php');

define('WP_ALLOW_MULTISITE', true);


define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', $_SERVER['HTTP_HOST']);
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

define('WPMU_PLUGIN_DIR',ROOT.'/addon/weiz/Common/Plugins');
define('MY_PLUGIN_DIR',WP_CONTENT_DIR);

define('WP_CONTENT_DIR', ROOT.'/addon');
define('WP_CONTENT_URL', '/addon');

define('WP_CORE_DIR', WP_CONTENT_DIR . '/ycms/wp-core');

define('COOKIEPATH', '/');

define('WP_PLUGIN_DIR', ROOT . '/addon');
define('WP_PLUGIN_URL', '/addon');
define('PLUGINDIR','addon');


/**
 * WordPress基础配置文件。
 *
 * 本文件包含以下配置选项：MySQL设置、数据库表名前缀、密钥、
 * WordPress语言设定以及ABSPATH。如需更多信息，请访问
 * {@link http://codex.wordpress.org/zh-cn:%E7%BC%96%E8%BE%91_wp-config.php
 * 编辑wp-config.php}Codex页面。MySQL设置具体信息请咨询您的空间提供商。
 *
 * 这个文件被安装程序用于自动生成wp-config.php配置文件，
 * 您可以手动复制这个文件，并重命名为“wp-config.php”，然后填入相关信息。
 *
 * @package WordPress
 */

// ** MySQL 设置 - 具体信息来自您正在使用的主机 ** //
/** WordPress数据库的名称 */
define('DB_NAME', env('DB_DATABASE','weiz'));

/** MySQL数据库用户名 */
define('DB_USER', env('DB_USERNAME','weiz'));

/** MySQL数据库密码 */
define('DB_PASSWORD', env('DB_PASSWORD','brFhPTBc5ZX7brYj'));

/** MySQL主机 */
define('DB_HOST', env('DB_HOST','localhost'));

/** 创建数据表时默认的文字编码 */
define('DB_CHARSET', env('DB_CHARSET','utf8'));

/** 数据库整理类型。如不确定请勿更改 */
define('DB_COLLATE', '');

/**#@+
 * 身份认证密钥与盐。
 *
 * 修改为任意独一无二的字串！
 * 或者直接访问{@link https://api.wordpress.org/secret-key/1.1/salt/
 * WordPress.org密钥生成服务}
 * 任何修改都会导致所有cookies失效，所有用户将必须重新登录。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'IU:,4Hre5<69s[$+n~7Xr|`2}5sFe. t~5WyJ;1r&@ @:7YZBl|xF2&bA~|XTWEL');
define('SECURE_AUTH_KEY',  'kJCY]|U^T&,CRIz.|R9)lwd49l+=J40U+h;txw2w||>Vs0_H{|G}s+*cd<tn-$Pn');
define('LOGGED_IN_KEY',    'u<B%O-37zNrAjkz@gEp2fgnpx[@?X;v*ry&57wAd+,Mc,*Cf|[!%t3Z&dQ<0FK@c');
define('NONCE_KEY',        ')837Xgar-Zl<HXd!R8/P+[#FXr w+~wHvuiEL71p~3R+5lD<V]1b[z,0%^*J&-g-');
define('AUTH_SALT',        '->gvKPBRHVHA-|7d%L(XaDLgq[*a,XA(-T!S;2iK[X*bX,|Ub?w~+IKo GyKug^j');
define('SECURE_AUTH_SALT', 'B}N(EsokkBLpwRC3#,A4C0.*n_vAH7Z_%)p)JS%qJdpO1gBPgA?/#Ar[yvpPcH~]');
define('LOGGED_IN_SALT',   'qlZG}B!S)pP-m06n.Ajl]+,;6c2d%2P+=oM87!o)*uzK~o+Soa.m|-BGQQYBzQN~');
define('NONCE_SALT',       'h4z@J0RGEK!ap`VydGiYxr+QW3#7~8&diM&c}00-/ay#Jt ;-NkIq%Gu<,+T|<pz');

/**#@-*/

/**
 * WordPress数据表前缀。
 *
 * 如果您有在同一数据库内安装多个WordPress的需求，请为每个WordPress设置
 * 不同的数据表前缀。前缀名只能为数字、字母加下划线。
 */
$table_prefix  = 'wp_';

/**
 * 开发者专用：WordPress调试模式。
 *
 * 将这个值改为true，WordPress将显示所有用于开发的提示。
 * 强烈建议插件开发者在开发环境中启用WP_DEBUG。
 */
define('WP_DEBUG', false);

/**
 * zh_CN本地化设置：启用ICP备案号显示
 *
 * 可在设置→常规中修改。
 * 如需禁用，请移除或注释掉本行。
 */
define('WP_ZH_CN_ICP_NUM', true);

/* 好了！请不要再继续编辑。请保存本文件。使用愉快！ */

/** WordPress目录的绝对路径。 */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** 设置WordPress变量和包含文件。 */
//require_once(ABSPATH . 'wp-settings.php');
require_once(WP_CORE_DIR. '/includes/wp.php');

