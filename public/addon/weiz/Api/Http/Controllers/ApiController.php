<?php namespace Modules\Api\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class ApiController extends Controller
{
    private function checkSignature()
    {
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];

        $token = TOKEN;
        $tmpArr = array($token, $timestamp, $nonce);
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode($tmpArr);
        $tmpStr = sha1($tmpStr);

        if ($tmpStr == $signature) {
            return true;
        } else {
            return false;
        }
    }

    public function check()
    {
        if ($this->checkSignature()) {
            exit($_GET['echostr']);
        }
        return false;
    }

    public function index()
    {
        define('TOKEN', 'weizco_api_20150723');

        $this->check();


        return view('api::index');
    }

}