<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * This service provider is a great spot to register your various container
     * bindings with the application. As you can see, we are registering our
     * "Registrar" implementation here. You can add your own bindings too!
     *
     * @return void
     */
    public function register()
    {

        global $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;


        /** Loads the WordPress Environment and Template */
        define('WP_ONLY', defined('ABSPATH'));
        if (WP_ONLY == false) {
            require_once(ROOT . '/home/wp-blog-header.php');
        }

        foreach (Modules::all() as $name => $module) {
            if (Modules::find($name)->json()->type == 'theme') {
                register_theme_directory(dirname($module->getpath()));
                //kd($module->getpath());
            }
        }

        if (function_exists('get_template_directory')) {
            if (is_dir(get_template_directory() . '/Resources/views')) {
                View::addLocation(get_template_directory() . '/Resources/views');
                View::addNamespace('tpl', get_template_directory() . '/Resources/views');
            }


            View::addLocation(get_template_directory());
            View::addNamespace('default', get_template_directory());
        }


        $this->app->bind(
            'Illuminate\Contracts\Auth\Registrar',
            'App\Services\Registrar'
        );
    }

}
