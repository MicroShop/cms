<?php namespace App\Http\Controllers;

use App\Http\Requests;

class WPController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        global $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;

        //
        if (WP_ONLY == false) {

            add_filter('template_redirect', function () {
                global $wp_query;

                if (is_404()) {
                    $url = "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
                    if (preg_match("/^(.+)\/edit\/*$/", $url, $m)) {
                        $url = $m[1];
                        $postid = url_to_postid($url);
                        if (!$postid && $page = get_page_by_path($url)) {
                            $postid = $page->ID;
                        }
                        if ($postid) {
                            header('location:' . home_url() . "/wp-admin/post.php?post={$postid}&action=edit");
                            exit;
                        }
                    }
                    status_header(200);
                    $wp_query->is_404 = false;
                }


            });

            define('WP_USE_THEMES', true);
            require(WP_CORE_DIR . '/includes/template-loader.php');
            return '';
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public
    function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public
    function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public
    function show(
        $id
    ) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public
    function edit(
        $id
    ) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public
    function update(
        $id
    ) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public
    function destroy(
        $id
    ) {
        //
    }

}
